using System;

namespace RollNut.NutMVVM
{
    /// <summary>
    /// Command which executes given delegate (if CanExecute is true).
    /// </summary>
    public class RelayCommand : CommandBase
    {
        // Fields

        private readonly Action<object> _execute;
        private readonly Predicate<object> _canExecute;

        // Constructors

        /// <summary>
        /// Creates a new command.
        /// </summary>
        /// <param name="owner">The ViewModel which is responsible for this command. This is usefull for logging, debugging and finding errors.</param>
        /// <param name="execute">The execution logic.</param>
        /// <param name="canExecute">The can execution logic. If null then execute is always possible.</param>
        public RelayCommand(IViewModel owner, Action<object> execute, Predicate<object> canExecute = null)
            : base(owner)
        {
            _execute = execute ?? throw new ArgumentNullException(nameof(execute));
            _canExecute = canExecute;
        }

        // Methods

        protected override bool OnCanExecute(object parameter)
        {
            // Can execute is true if no delegate exists or the result is true.
            bool canExecuteState = _canExecute == null || _canExecute(parameter);
            return canExecuteState;
        }
        
        protected override void OnExecute(object parameter)
        {
            _execute(parameter);
        }
    }
}