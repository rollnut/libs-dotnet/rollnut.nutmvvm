using System;
using System.Windows.Input;

namespace RollNut.NutMVVM
{
    /// <summary>
    /// Base class for commands.
    /// </summary>
    public abstract class CommandBase : NotifyBase, ICommand
    {
        // Constructors

        /// <param name="owner">The ViewModel which is responsible for this command.</param>
        public CommandBase(IViewModel owner)
        {
            Owner = owner ?? throw new ArgumentNullException(nameof(owner));
        }

        // Properties

        /// <summary>
        /// The ViewModel which is responsible for this command. 
        /// This is usefull for logging, debugging and finding errors.
        /// </summary>
        public IViewModel Owner { get; private set; }

        /// <summary>
        /// The event <see cref="CanExecuteChanged"/> is only raised if the
        /// current result of CanExecute differs to the last one.
        /// </summary>
        protected bool? LastValueOfCanExecute { get; set; }

        // Properties - Display to User

        /// <summary>
        /// The user-friendly caption of this command.
        /// </summary>
        public string Caption { get; set; }

        /// <summary>
        /// The user-friendly description for this command.
        /// </summary>
        public string Description { get; set; }

        private string _cannotExcecuteReason;
        /// <summary>
        /// The user-friendly reason why CanExecute returns false.
        /// </summary>
        public string CannotExcecuteReason
        {
            get { return _cannotExcecuteReason; }
            set { SetValue(ref _cannotExcecuteReason, value); }
        }

        // Events

        /// <summary>
        /// Will be raised if the can execute state changed.
        /// </summary>
        public event EventHandler CanExecuteChanged;

        /// <summary>
        /// Raise the can execute changed event
        /// but only if the given value is different than the value from the last raising.
        /// <para>
        /// In order to force raising the event set null to <see cref="LastValueOfCanExecute"/>.
        /// </para>
        /// </summary>
        protected void OnCanExecuteChanged(bool canExecute)
        {
            if (canExecute == LastValueOfCanExecute)
            {
                return;
            }

            LastValueOfCanExecute = canExecute;
            CanExecuteChanged?.Invoke(this, EventArgs.Empty);
        }

        // Methods - CanExecute

        /// <summary>
        /// Return true if execute is possible.
        /// Caching behaviour is not required by child class.
        /// </summary>
        protected abstract bool OnCanExecute(object parameter);

        /// <summary>
        /// Return true if execute is possible.
        /// The event <see cref="CanExecuteChanged"/> is raised
        /// but only if the state differs from last raising.
        /// </summary>
        public bool CanExecute(object parameter)
        {
            bool canExecuteState = OnCanExecute(parameter);
            OnCanExecuteChanged(canExecuteState);

            return canExecuteState;
        }

        // Methods - Execute

        /// <summary>
        /// The commands execution logic.
        /// </summary>
        protected abstract void OnExecute(object parameter);

        /// <summary>
        /// Execute the command but only if possible (CanExecute must be true).
        /// </summary>
        public void Execute(object parameter)
        {
            // If Execute is not possible do nothing!
            if (CanExecute(parameter))
            {
                OnExecute(parameter);

                // After executing the Can-State can be changed.
                // Call to this method will raise the event if required
                // which notifies all event listeners.
                CanExecute(parameter);
            }
        }

        public override string ToString()
        {
            return $"{GetType().Name} on {Owner.GetType().Name} ({Caption})";
        }

        /// <summary>
        /// Ignore performance and force raising CanExecuteChanged-Event.
        /// </summary>
        public static void ForceRaiseCanExecuteChangedEvent(CommandBase command, object parameter)
        {
            if (command is null) throw new ArgumentNullException(nameof(command));

            command.LastValueOfCanExecute = null;
            command.CanExecute(parameter);
        }
    }
}