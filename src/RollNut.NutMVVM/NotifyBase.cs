﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace RollNut.NutMVVM
{
    public class NotifyBase : INotifyPropertyChanging, INotifyPropertyChanged
    {
        // Events - PropertyChanging

        public event PropertyChangingEventHandler PropertyChanging;

        protected virtual void OnPropertyChanging(string propertyName)
        {
            PropertyChanging?.Invoke(this, new PropertyChangingEventArgs(propertyName));
        }

        // Events - PropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Raises this object's PropertyChanged event 
        /// (if raise property changed is not in suppress mode).
        /// </summary>
        /// <param name="propertyName">The property that has a new value.</param>
        [DebuggerStepThrough]
        protected virtual void OnPropertyChanged(string propertyName)
        {
            // If Log.TraceLevel is true then Check if propertyName exists. Or at warning level?

            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

            // // Check if given name is valid.
            // VerifyPropertyName(propertyName);

            // // If suppress is active do not raise property changed.
            // if (_suppressPropertyChangedEvent)
            // {
            //     // Store the changed property to raise it later.
            //     if (!_suppressedPropertyChangedNames.Contains(propertyName))
            //         _suppressedPropertyChangedNames.Add(propertyName);
            // }
            // else if (this.PropertyChanged != null)
            // {
            //     // Raise property changed.
            //     this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            // }
        }

        // .:: Methods - Usefull for Binding

        /// <summary>
        /// A shorter way for binding properties. Return false if property was not updated.
        /// </summary>
        /// <param name="propertyValueHolder">The property field.</param>
        /// <param name="newValue">The new value to assign.</param>
        /// <param name="propertyName">The property name as string.</param>
        [DebuggerStepThrough]
        protected virtual bool SetValue<T>(ref T backingField, T newValue, [CallerMemberName] string propertyName = null)
        {
            // If current and new value are equal do not update.
            if ((backingField == null && newValue == null)
                || (backingField != null && backingField.Equals(newValue))) return false;

            if (EqualityComparer<T>.Default.Equals(backingField, newValue))
            {
                return false;
            }

            // Set new value and raise property changed.
            OnPropertyChanging(propertyName);
            backingField = newValue;
            OnPropertyChanged(propertyName);

            return true;
        }

        /// <summary>
        /// Raise property changed for each public property by reflection.
        /// Suppress mode is ignored.
        /// </summary>
        protected void RaisePropertyChangedAll()
        {
            // foreach (var prop in GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance).Where(x => x.CanRead))
            // {
            //     OnPropertyChanged(prop.Name);
            // }
        }



         // Methods - Helper

        /// <summary>
        /// Throw an exception when no public property with given name can be found.
        /// Check only if CanVerifyPropertyName-Property is true.
        /// </summary>
        //[Conditional("DEBUG")]
        [DebuggerStepThrough]
        private void VerifyPropertyName(string propertyName)
        {
            // TODO: Maybee cache all relevant property names as hashset.
            // if (this.CanVerifyPropertyName && !string.IsNullOrEmpty(propertyName))
            // {
            //     var property = GetType().GetProperty(propertyName);

            //     // Verify that the property name matches a real, public, instance property on this object.
            //     if (property == null || !property.CanRead)
            //     {
            //         throw new NotifyPropertyNotFoundException(GetType(), propertyName);
            //     }
            // }
        }
    }
}
