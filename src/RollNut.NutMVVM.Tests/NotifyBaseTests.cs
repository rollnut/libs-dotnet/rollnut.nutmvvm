using Xunit;

namespace RollNut.NutMVVM.Tests
{
    public class NotifyBaseTests
    {
        private class NotifyDummy : NotifyBase
        {
            private int intProperty;
            private string stringProperty;

            public int IntProperty { get => intProperty; set => SetValue(ref intProperty, value); }
            public string StringProperty { get => stringProperty; set => SetValue(ref stringProperty, value); }
        }

        [Fact]
        public void OnPropertyChanged()
        {
            string nameByChanging = "";
            string nameByChanged = "";

            var dummy = new NotifyDummy();
            dummy.PropertyChanging += (s, e) => nameByChanging = e.PropertyName;
            dummy.PropertyChanged += (s, e) => nameByChanged = e.PropertyName;

            dummy.IntProperty = 5;

            Assert.Equal(nameof(dummy.IntProperty), nameByChanging);
            Assert.Equal(nameof(dummy.IntProperty), nameByChanging);
        }
    }
}