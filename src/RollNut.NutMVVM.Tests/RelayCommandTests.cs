using System;
using Moq;
using Xunit;

namespace RollNut.NutMVVM.Tests
{
    public class RelayCommandTests
    {
        [Fact]
        public void Execute_HasExecuted()
        {
            var ownerMock = new Mock<IViewModel>().Object;

            bool executed = false;

            var command = new RelayCommand(ownerMock, o => executed = true);
            command.Execute(null);

            Assert.True(executed);
        }

        [Fact]
        public void Execute_CanExecuteTrue_HasExecuted()
        {
            var ownerMock = new Mock<IViewModel>().Object;

            bool executed = false;

            var command = new RelayCommand(ownerMock, o => executed = true, o => true);
            command.Execute(null);

            Assert.True(executed);
        }

        [Fact]
        public void Execute_CanExecuteFalse_HasNotExecuted()
        {
            var ownerMock = new Mock<IViewModel>().Object;

            bool executed = false;

            var command = new RelayCommand(ownerMock, o => executed = true, o => false);
            command.Execute(null);

            Assert.False(executed);
        }

        [Fact]
        public void TestIf_Execute_CanExecute_And_CanExecuteChanged_IsCorrectlyCalled()
        {
            var ownerMock = new Mock<IViewModel>().Object;

            int expectedCalledCountOfExecute = 4;            
            int expectedCalledCountOfCanExecute = 11; // <- Each Execute call will call CanExecute one or two times.
            int expectedCalledCountOfCanExecuteChanged = 1;

            int actualCalledCountOfExecute = 0;
            int actualCalledCountOfCanExecute = 0;
            int actualCalledCountOfCanExecuteChanged = 0;

            var command = new RelayCommand(ownerMock
                , o => { actualCalledCountOfExecute += 1; }
                , o => 
                { 
                    actualCalledCountOfCanExecute += 1;
                    return true;
                }
            );

            command.CanExecuteChanged += (s, e) => actualCalledCountOfCanExecuteChanged += 1;

            command.Execute(null);
            command.Execute(null);

            command.CanExecute(null);
            command.CanExecute(null);

            command.Execute(null);
            command.CanExecute(null);
            command.Execute(null);

            Assert.Equal(expectedCalledCountOfExecute, actualCalledCountOfExecute);
            Assert.Equal(expectedCalledCountOfCanExecute, actualCalledCountOfCanExecute);
            Assert.Equal(expectedCalledCountOfCanExecuteChanged, actualCalledCountOfCanExecuteChanged);
        }

        [Fact]
        public void Execute_EnsureCachingBehaviourWorksForCanExecuteChanged()
        {
            var ownerMock = new Mock<IViewModel>().Object;

            int expectedCalledCountOfCanExecuteChanged = 5;
            int actualCalledCountOfCanExecuteChanged = 0;

            var command = new RelayCommand(ownerMock
                , o => {}
                , o => (bool)o
            );
                
            command.CanExecuteChanged += (s, e) => actualCalledCountOfCanExecuteChanged += 1;

            // Expected Changed-Event
            command.CanExecute(false);
            command.CanExecute(false);

            // Expected Changed-Event
            command.CanExecute(true);

            // Expected Changed-Event
            command.CanExecute(false);

            // Expected Changed-Event
            command.CanExecute(true);
            command.CanExecute(true);
            command.CanExecute(true);

            // Expected Changed-Event
            command.CanExecute(false);
            command.CanExecute(false);
            command.CanExecute(false);
            
            Assert.Equal(expectedCalledCountOfCanExecuteChanged, actualCalledCountOfCanExecuteChanged);
        }

        [Fact]
        public void Execute_CallExecutionLogicOnlyIfCanExecuteIsTrue()
        {
            var ownerMock = new Mock<IViewModel>().Object;

            int expectedCalledCountOfExecute = 4;

            int actualCalledCountOfExecute = 0;

            var command = new RelayCommand(ownerMock
                , o => actualCalledCountOfExecute++
                , o => (bool)o
            );

            command.Execute(true);
            command.Execute(true);

            command.Execute(false);
            command.Execute(false);

            command.Execute(true);
            command.Execute(false);
            command.Execute(true);

            Assert.Equal(expectedCalledCountOfExecute, actualCalledCountOfExecute);
        }
    }
}